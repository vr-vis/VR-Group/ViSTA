#!/bin/bash

# path to the executable directory (relative to this script's path)
EXECUTABLE_PATH=..
# path to the working dir that should be set before statrting the program (relative to this script's path)
WORKINGDIR_PATH=..

# names of the executables (filenames only, without path components, must be located in EXECUTABLE_PATH)
EXECUTABLE_NAME_RELEASE=MyApplication
EXECUTABLE_NAME_DEBUG=MyApplicationD

# names of the path script (relative to executable path)
PATH_SCRIPT=set_path_for_MyApplication.sh

# additional output that is printed by run.sh -h, or if errors occur
# should contain all optional parameters that are parsed in run.sh
print_additional_usage_output()
{
	echo "                -D --debug"
	echo "                     runs all instances in debug mode"
	echo "                -DM --debug_master"
	echo "                     runs master/standalone instances in debug mode"
	echo "                -DS --debug_slave"
	echo "                     run slave instances in debug mode"
	echo "                -TV --totalview"
	echo "                     starts debugging with totalview (on master/standalone)"
	echo "                -TVS --totalview_slaves"
	echo "                     starts debugging with totalview (on slaves)"
	echo "                -VG --valgrind"
	echo "                     starts profiling with valgrind (on master/standalone)"
	echo "                -VGS --valgrind_slaves"
	echo "                     starts profiling with valgrind (on slaves)"
}

