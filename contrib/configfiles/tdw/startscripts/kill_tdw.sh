#!/bin/bash

# generic script to kill an application on the tiled display wall at the RWTH Aachen University
# requires APPLICATION_NAME_RELEASE and APPLICATION_NAME_DEBUG

########################################
# Generic, application-independet file #
# Don't edit!                          #
# (unless you know what you're doing)  #
########################################

killall $APPLICATION_NAME_RELEASE > /dev/null 2>&1 &
killall $APPLICATION_NAME_DEBUG > /dev/null 2>&1 &
/home/vrsw/tiled_display_wall/bin/kill_tdw_nowait.sh $APPLICATION_NAME_RELEASE > /dev/null 2>&1 &
/home/vrsw/tiled_display_wall/bin/kill_tdw_nowait.sh $APPLICATION_NAME_DEBUG > /dev/null 2>&1 &
